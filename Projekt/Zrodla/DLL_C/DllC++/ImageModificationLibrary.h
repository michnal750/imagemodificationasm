#pragma once

#ifdef IMAGEMODIFICATIONLIBRARY_EXPORTS
#define IMAGEMODIFICATIONLIBRARY_API __declspec(dllexport)
#else
#define IMAGEMODIFICATIONLIBRARY_API __declspec(dllimport)
#endif


extern "C" IMAGEMODIFICATIONLIBRARY_API void modifyImageCpp(unsigned char* packedBytesVector, float colorValueX);