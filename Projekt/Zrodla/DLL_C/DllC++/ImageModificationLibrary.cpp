
#include "pch.h"
#include "ImageModificationLibrary.h"
#include <immintrin.h>

void modifyImageCpp(unsigned char* packedBytesVector, float colorValue)
{
	//utworzenie packedfloat32 z jednej wartosci przekazanej jako argument
	__m128 colorValueVector = _mm_set1_ps(colorValue);

	//zaladowanie wektora znajdujacego sie pod przekazanym adresem
	__m128i vector0 = _mm_lddqu_si128((__m128i*)packedBytesVector);

	__m128i vector1i = _mm_cvtepu8_epi32(vector0); //rozpakowanie packedint8 do 4 wektorow packedint32 
	vector0 = _mm_bsrli_si128(vector0, 4); //przesuniecie o 4 bajty w prawo
	__m128i vector2i = _mm_cvtepu8_epi32(vector0); 
	vector0 = _mm_bsrli_si128(vector0, 4);
	__m128i vector3i = _mm_cvtepu8_epi32(vector0);
	vector0 = _mm_bsrli_si128(vector0, 4);
	__m128i vector4i = _mm_cvtepu8_epi32(vector0);

	//konwersja packedint32 do packedfloat32
	__m128 vector1f = _mm_cvtepi32_ps(vector1i);
	__m128 vector2f = _mm_cvtepi32_ps(vector2i); 
	__m128 vector3f = _mm_cvtepi32_ps(vector3i); 
	__m128 vector4f = _mm_cvtepi32_ps(vector4i); 

	//mnozenie 
	vector1f = _mm_mul_ps(vector1f, colorValueVector);
	vector2f = _mm_mul_ps(vector2f, colorValueVector);
	vector3f = _mm_mul_ps(vector3f, colorValueVector);
	vector4f = _mm_mul_ps(vector4f, colorValueVector);

	//konwersja packedfloat32 do packedint32
	vector1i = _mm_cvtps_epi32(vector1f); 
	vector2i = _mm_cvtps_epi32(vector2f); 
	vector3i = _mm_cvtps_epi32(vector3f); 
	vector4i = _mm_cvtps_epi32(vector4f); 

	//ustawienie masek do szuflowania
	__m128i mask1 = _mm_set_epi8(12, 8, 4, 0, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128);
	__m128i mask2 = _mm_set_epi8(128, 128, 128, 128, 12, 8, 4, 0, 128, 128, 128, 128, 128, 128, 128, 128);
	__m128i mask3 = _mm_set_epi8(128, 128, 128, 128, 128, 128, 128, 128, 12, 8, 4, 0, 128, 128, 128, 128);
	__m128i mask4 = _mm_set_epi8(128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 12, 8, 4, 0);

	//szuflowanie i laczenie 4 wektorow w jeden
	 vector1i = _mm_or_si128(_mm_shuffle_epi8(vector1i, mask4), _mm_shuffle_epi8(vector2i, mask3));
	 vector3i = _mm_or_si128(_mm_shuffle_epi8(vector3i, mask2), _mm_shuffle_epi8(vector4i, mask1));
	 vector1i = _mm_or_si128(vector1i, vector3i);

	 //zaladowanie wektora pod przekazany adres
	 _mm_storeu_si128((__m128i*)packedBytesVector, vector1i);
}