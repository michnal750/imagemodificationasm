﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AssemblerProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly ModifyPhotoPage photoEditorPage;
        private readonly SettingsPage settingsPage;
        private readonly InformationPage informationPage;

        public MainWindow()
        {
            Data data = new Data();
            InitializeComponent();
            informationPage = new InformationPage();
            photoEditorPage = new ModifyPhotoPage(data);
            settingsPage = new SettingsPage(data);
            mainGrid.Children.Add(photoEditorPage);
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e) => Application.Current.Shutdown();

        private void MinimalizeButton_Click(object sender, RoutedEventArgs e) => WindowState = WindowState.Minimized;

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e) => DragMove();

        private void MenuListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = MenuListView.SelectedIndex;
            MoveCursorMenu(index);

            switch(index)
            {
                case 0:
                    mainGrid.Children.Clear();
                    mainGrid.Children.Add(photoEditorPage);
                break;
                case 1:
                    mainGrid.Children.Clear();
                    mainGrid.Children.Add(settingsPage);
                break;
                case 2:
                    mainGrid.Children.Clear();
                    mainGrid.Children.Add(informationPage);
                    break;

                default:
                    mainGrid.Children.Clear();
                break;
            }
        }

        private void MoveCursorMenu(int index)
        {
           TransitioningContentSlide.OnApplyTemplate();
           cursorGrid.Margin = new Thickness(0, cursorGrid.ActualHeight * index + 60 + (index * 20), 0, 0);
        }
    }
}
