﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using System.Drawing;
using System.Windows.Interop;
using System.Threading;

namespace AssemblerProject
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class ModifyPhotoPage : UserControl
    {       
        private Bitmap bitmap;
        private Bitmap newBitmap;
        private Data data;
        private Thread thread1;

        public ModifyPhotoPage(Data data)
        {
            this.data = data;
            thread1 = new Thread(new ThreadStart(ModifyImage));
            InitializeComponent();
        }

        private void LoadPhotoButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Select a picture";
            openFileDialog.Filter = "All supported graphics|*.jpg;*.jpeg;*.png;*.bmp|" +
              "JPEG |*.jpg;*.jpeg|" +
              "Bitmap Image | *.bmp|" +
              "Portable Network Graphic |*.png";
            if (openFileDialog.ShowDialog() == true)
            {
                NoFileLoadedText.Text = "";
                MainImage.Source = new BitmapImage(new Uri(openFileDialog.FileName));

                bitmap = new Bitmap(openFileDialog.FileName);
                MainImage.Source = Imaging.CreateBitmapSourceFromHBitmap(bitmap.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            }
        }

        private void ModifyPhotoButton_Click(object sender, RoutedEventArgs e)
        {
            if (bitmap != null && !thread1.IsAlive)
            {
                thread1 = new Thread(new ThreadStart(ModifyImage));
                thread1.Start();
            }
        }
        private void ModifyImage()
        {
            data.time = 0;
            BitmapTransformation bitmapTransformation = new BitmapTransformation(data, bitmap);
            newBitmap = bitmapTransformation.TransformBitmap();
            this.Dispatcher.Invoke(() =>
            { 
                MainImage.Source = Imaging.CreateBitmapSourceFromHBitmap(newBitmap.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                timerText.Text = data.time.ToString() + " ms";
            }
                                   );
        }

        private void SavePhotoButton_Click(object sender, RoutedEventArgs e)
        {
            if (newBitmap != null)
            {
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.FileName = "ModifiedImage";
                dlg.Filter = "JPG Image|*.jpg|Bitmap Image|*.bmp |JPEG Image |*.jpeg |Png Image |*.png";
                if (dlg.ShowDialog() == true)
                {
                    var encoder = new JpegBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(Imaging.CreateBitmapSourceFromHBitmap(newBitmap.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions())));
                    using (var stream = dlg.OpenFile())
                    {
                        encoder.Save(stream);
                    }
                }
            }
        }
    }
}
