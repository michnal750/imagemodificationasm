﻿using System.Collections.Generic;

namespace AssemblerProject
{
    public class Data
    {
        public bool asmDll { get; set; }
        public double time { get; set; }
        public int numberOfThreads { get; set; }
        public List<ColorPercentageValue> strapsColorPercentageValue;

        public Data()
        {
            strapsColorPercentageValue = new List<ColorPercentageValue>() 
            { 
              new ColorPercentageValue(),
              new ColorPercentageValue(),
              new ColorPercentageValue(),
              new ColorPercentageValue(),
              new ColorPercentageValue(),
              new ColorPercentageValue()
            };
        }
    }

    public class ColorPercentageValue
    {
        public float A { get; set; }
        public float R { get; set; }
        public float G { get; set; }
        public float B { get; set; }
    }
}
