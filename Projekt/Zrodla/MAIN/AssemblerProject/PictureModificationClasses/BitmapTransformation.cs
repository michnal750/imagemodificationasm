﻿using System.Drawing;
using System;
using System.Runtime.InteropServices;

namespace AssemblerProject
{
    class BitmapTransformation
    {
        private Data data;
        private Bitmap oldBitmap;
        private Bitmap newBitmap;
        private byte[] aByteArray;
        private byte[] rByteArray;
        private byte[] gByteArray;
        private byte[] bByteArray;

        private readonly int numberOfStraps = 6;
        private readonly int numberOfPixelsInVector = 16;

        public BitmapTransformation(Data data, Bitmap oldBitmap)
        {
            this.data = data;
            this.oldBitmap = oldBitmap;
            this.newBitmap = new Bitmap(oldBitmap.Width, oldBitmap.Height);
        }

        public Bitmap TransformBitmap()
        {
            int stripeHeight = oldBitmap.Height / numberOfStraps;
            int stripeWidth = newBitmap.Width;
            int actualLine = 0;
                for (int i = 0; i < numberOfStraps; i++)
                {
                    if (i == numberOfStraps - 1) stripeHeight = oldBitmap.Height - (numberOfStraps - 1) * stripeHeight;
                    ChangeOneStripeColor(stripeHeight, stripeWidth, actualLine, i);
                    actualLine += stripeHeight;
                }
            return newBitmap;
        }

        private void ChangeOneStripeColor(int stripeHeight, int stripeWidth, int actualLine, int actualStripe)
        {
            int size = stripeHeight * stripeWidth;
            // powiekszanie tablicy aby mozna bylo utworzyc rowne wektory
            while (size % numberOfPixelsInVector != 0)
            {
                ++size;
            }

            aByteArray = new byte[size];
            rByteArray = new byte[size];
            gByteArray = new byte[size];
            bByteArray = new byte[size];

            int i = 0;
            int y;
            for (y = actualLine; y < actualLine + stripeHeight; y++)
            {
                for (int x = 0; x < stripeWidth; x++)
                {
                    aByteArray[i] = oldBitmap.GetPixel(x, y).A;
                    rByteArray[i] = oldBitmap.GetPixel(x, y).R;
                    gByteArray[i] = oldBitmap.GetPixel(x, y).G;
                    bByteArray[i] = oldBitmap.GetPixel(x, y).B;
                    ++i;
                }
            }

            // transformByteArrayToPtr

            IntPtr[] rByteArrayPtr = new IntPtr[size / numberOfPixelsInVector];
            IntPtr[] gByteArrayPtr = new IntPtr[size / numberOfPixelsInVector];
            IntPtr[] bByteArrayPtr = new IntPtr[size / numberOfPixelsInVector];
            int it2 = 0;
            for (int it1 = 0; it1 < size / numberOfPixelsInVector; it1++)
            {
                rByteArrayPtr[it1] = Marshal.AllocHGlobal(sizeof(byte) * numberOfPixelsInVector);
                Marshal.Copy(rByteArray, it2, rByteArrayPtr[it1], numberOfPixelsInVector);
                gByteArrayPtr[it1] = Marshal.AllocHGlobal(sizeof(byte) * numberOfPixelsInVector);
                Marshal.Copy(gByteArray, it2, gByteArrayPtr[it1], numberOfPixelsInVector);
                bByteArrayPtr[it1] = Marshal.AllocHGlobal(sizeof(byte) * numberOfPixelsInVector);
                Marshal.Copy(bByteArray, it2, bByteArrayPtr[it1], numberOfPixelsInVector);
                it2 += numberOfPixelsInVector;
            }
            ThreadsManagement threadsManagement = new ThreadsManagement(data, rByteArrayPtr, gByteArrayPtr, bByteArrayPtr);
            threadsManagement.ModifyPhoto(actualStripe);
            it2 = 0;
            for (int it1 = 0; it1 < size / numberOfPixelsInVector; it1++)
            {
                Marshal.Copy(rByteArrayPtr[it1], rByteArray, it2, numberOfPixelsInVector);
                Marshal.FreeHGlobal(rByteArrayPtr[it1]);
                Marshal.Copy(gByteArrayPtr[it1], gByteArray, it2, numberOfPixelsInVector);
                Marshal.FreeHGlobal(gByteArrayPtr[it1]);
                Marshal.Copy(bByteArrayPtr[it1], bByteArray, it2, numberOfPixelsInVector);
                Marshal.FreeHGlobal(bByteArrayPtr[it1]);
                it2 += numberOfPixelsInVector;
            }

            // transformByteArrayToBitmap

            System.Drawing.Color newColor;
            int it = 0;
            for (y = actualLine; y < actualLine + stripeHeight; y++)
            {
                for (int x = 0; x < stripeWidth; x++)
                {
                    newColor = System.Drawing.Color.FromArgb(aByteArray[it], rByteArray[it], gByteArray[it], bByteArray[it]);
                    newBitmap.SetPixel(x, y, newColor);
                    ++it;
                }
            }
        }
    }
}
