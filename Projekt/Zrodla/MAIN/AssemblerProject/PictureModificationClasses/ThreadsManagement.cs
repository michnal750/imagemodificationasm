﻿using System;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace AssemblerProject
{
    public delegate void ModifyImage(IntPtr intPtr, float colorValue);

    class ThreadsManagement
    {
        [DllImport("ASMDLL.dll")]
        public static extern void modifyImageAsm(IntPtr intPtr, float colorValue);

        [DllImport("DllC++.dll")]
        public static extern void modifyImageCpp(IntPtr intPtr, float colorValue);

        private IntPtr[] rByteArrayPtr;
        private IntPtr[] gByteArrayPtr;
        private IntPtr[] bByteArrayPtr;

        private Data data;
        private ModifyImage modifyImage;

        public ThreadsManagement(Data data, IntPtr[] rByteArrayPtr, IntPtr[] gByteArrayPtr, IntPtr[] bByteArrayPtr)
        {
            this.data = data;
            this.rByteArrayPtr = rByteArrayPtr;
            this.gByteArrayPtr = gByteArrayPtr;
            this.bByteArrayPtr = bByteArrayPtr;

            if(data.asmDll) modifyImage = new ModifyImage(modifyImageAsm);
            else modifyImage = new ModifyImage(modifyImageCpp);
        }
        public void ModifyPhoto(int actualStrape)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            Parallel.For(0, rByteArrayPtr.Length, new ParallelOptions { MaxDegreeOfParallelism = data.numberOfThreads }, i =>
             {
                 modifyImage(rByteArrayPtr[i], data.strapsColorPercentageValue[actualStrape].R);
                 modifyImage(gByteArrayPtr[i], data.strapsColorPercentageValue[actualStrape].G);
                 modifyImage(bByteArrayPtr[i], data.strapsColorPercentageValue[actualStrape].B);
             }
                        );
            stopwatch.Stop();
            data.time += stopwatch.Elapsed.TotalMilliseconds;
        }       
    }
}
