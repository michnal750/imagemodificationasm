﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace AssemblerProject
{
    /// <summary>
    /// Interaction logic for SettingsPage.xaml
    /// </summary>
    public partial class SettingsPage : UserControl
    {
        private readonly int maxValue = 64;
        private Data data;
        public SettingsPage(Data data)
        {
            this.data = data;
            int numberOfCores = Environment.ProcessorCount;
            InitializeComponent();
            slider.Value = numberOfCores;
        }

        private void slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            sliderText.Text = e.NewValue.ToString();
            data.numberOfThreads = (int)e.NewValue;
        }

        private void sliderText_TextChanged(object sender, TextChangedEventArgs e)
        {
            int newValue;
            int oldValue = (int)slider.Value;
            if (Int32.TryParse(sliderText.Text, out newValue))
            {
                slider.Value = newValue <= maxValue ? newValue : 64;
                data.numberOfThreads = newValue;
            }
            else
            {
                slider.Value = oldValue;
                data.numberOfThreads = oldValue;
            }
        }

        private void cp1_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            data.strapsColorPercentageValue[0].A = (float)cp1.SelectedColor.Value.A / 255.0f;
            data.strapsColorPercentageValue[0].R = cp1.SelectedColor.Value.R / 255.0f;
            data.strapsColorPercentageValue[0].G = cp1.SelectedColor.Value.G / 255.0f;
            data.strapsColorPercentageValue[0].B = cp1.SelectedColor.Value.B / 255.0f;
        }

        private void cp2_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            data.strapsColorPercentageValue[1].A = cp2.SelectedColor.Value.A / 255.0f;
            data.strapsColorPercentageValue[1].R = cp2.SelectedColor.Value.R / 255.0f;
            data.strapsColorPercentageValue[1].G = cp2.SelectedColor.Value.G / 255.0f;
            data.strapsColorPercentageValue[1].B = cp2.SelectedColor.Value.B / 255.0f;
        }

        private void cp3_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            data.strapsColorPercentageValue[2].A = cp3.SelectedColor.Value.A / 255.0f;
            data.strapsColorPercentageValue[2].R = cp3.SelectedColor.Value.R / 255.0f;
            data.strapsColorPercentageValue[2].G = cp3.SelectedColor.Value.G / 255.0f;
            data.strapsColorPercentageValue[2].B = cp3.SelectedColor.Value.B / 255.0f;
        }

        private void cp4_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            data.strapsColorPercentageValue[3].A = cp4.SelectedColor.Value.A / 255.0f;
            data.strapsColorPercentageValue[3].R = cp4.SelectedColor.Value.R / 255.0f;
            data.strapsColorPercentageValue[3].G = cp4.SelectedColor.Value.G / 255.0f;
            data.strapsColorPercentageValue[3].B = cp4.SelectedColor.Value.B / 255.0f;
        }

        private void cp5_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            data.strapsColorPercentageValue[4].A = cp5.SelectedColor.Value.A / 255.0f;
            data.strapsColorPercentageValue[4].R = cp5.SelectedColor.Value.R / 255.0f;
            data.strapsColorPercentageValue[4].G = cp5.SelectedColor.Value.G / 255.0f;
            data.strapsColorPercentageValue[4].B = cp5.SelectedColor.Value.B / 255.0f;
        }

        private void cp6_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            data.strapsColorPercentageValue[5].A = cp6.SelectedColor.Value.A / 255.0f;
            data.strapsColorPercentageValue[5].R = cp6.SelectedColor.Value.R / 255.0f;
            data.strapsColorPercentageValue[5].G = cp6.SelectedColor.Value.G / 255.0f;
            data.strapsColorPercentageValue[5].B = cp6.SelectedColor.Value.B / 255.0f;
        }

        private void radioButton1_Checked(object sender, RoutedEventArgs e) => data.asmDll = true;
        private void radioButton2_Checked(object sender, RoutedEventArgs e) => data.asmDll = false;
    }
}
